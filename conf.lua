function love.conf(c)

	c.window.width = 640
	c.window.height = 480 
	c.window.depth = 4
	c.window.title = "Gear editor V0.1"
	c.window.icon = "icon.png"
	c.window.vsync = true
	c.window.fullscreen = false
	c.window.highdpi = false
	c.window.msaa = 0
	c.window.stencil = 4 
	c.window.usedpiscale = false
	c.window.resizable = false

end