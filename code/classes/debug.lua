-- *************************************
-- *                                   *
-- *              Debug                *
-- *                             Class *
-- *************************************
debugClass = {};function debugClass:new(o)
	
    -- Constructor
	local function constructor(o)
	
		o = o or {}
		
		-- Base properties
		o.name = "El" 
		
		-- Nested properties  
		
		-- Set metatable
		setmetatable(o, self)    
		self.__index = self

		
		return self

	end;self = constructor(o)

	-- *************************************
	-- *              Methods              *
	-- *************************************	
	-- Display camera data
	function self:displayCameraData(camera, x, y)

		-- Print camera data
		love.graphics.print("Camera X: "..tostring(camera.position.x), x, y)
		love.graphics.print("Camera Y: "..tostring(camera.position.y), x, y+20)
		love.graphics.print("Camera Z: "..tostring(camera.position.z), x, y+40)
		love.graphics.print("Camera X angle: "..tostring(camera.angle.x), x, y+60)
		love.graphics.print("Camera Y angle: "..tostring(camera.angle.y), x, y+80)
		love.graphics.print("Camera Z angle: "..tostring(camera.angle.z), x, y+100)
		love.graphics.print("Camera pitch: "..tostring(camera.angle.pitch), x, y+120)

	end

	-- Display gear name
	function self:displayGearName(x, y)

		-- Print gear name
		love.graphics.print(self.name, x, y)

	end

	function self:setName(name)
		self.name = name
	end
	
	return o

end