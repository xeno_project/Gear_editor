-- *************************************
-- *                                   *
-- *               Gear                *
-- *                             Class *
-- *************************************

require "code/classes/gearPart"

gearClass = {};function gearClass:new(o)
	
    -- Constructor
	local function constructor(o)
	
		o = o or {}
		
		-- Base properties
		o.name = ""
		-- Nested properties  
		o.limbs = {}
		o.position = {
			x=0,
			y=0,
			z=0
		}
		
		-- Set metatable
		setmetatable(o, self)    
		self.__index = self

		
		return self

	end;self = constructor(o)

	-- *************************************
	-- *              Methods              *
	-- *************************************	
	-- Initialize gear
	function self:init(scriptPath)

		-- load gear script
		local gearScript = self:load(scriptPath)

		-- create gear
		self:create(gearScript)

	end

	-- load gear script
	function self:load(scriptPath)
		return require(scriptPath)
	end

	-- create gear
	function self:create(gearScript)

		-- set gear name
		self.name = gearScript.name
		
		-- Load gear Texture
		local gearTexture = love.graphics.newImage("assets/models/gears/"..gearScript.texture)

		-- Filter gear texture to pixellize
		gearTexture:setFilter("nearest", "nearest", 1)

		-- load limbs
		for _, value in ipairs(gearScript.limbs) do

			-- create limb
			local limb = gearPartClass:new{lib = self.lib}

			-- initialize limb
			limb:init(
				value.name, value.objectName, gearTexture, 
				{value.position.x, value.position.y, value.position.z}, 
				gearScript.scale
			)

			-- add limb to limbs list
			self.limbs[value.id] = limb
		end

		-- set parent child relation
		for _, value in ipairs(gearScript.limbs) do

			if(value.parent ~= nil) then
				local parent = self.limbs[value.parent]
				parent:addChild(self.limbs[value.id])
			else
				-- root limb
				self.root = self.limbs[value.id]
			end
		end
	end

	-- rotate the limb at index i
	function self:rotateLimb(i, x, y, z)
		self.limbs[i]:transform(nil, nil, nil, x, y, z)
	end

	-- move gear
	function self:moveGear(x, y, z)
		self.root:transform(x + self.root.position.x, 
			y + self.root.position.y, 
			z + self.root.position.z, 
			0, 0, 0
		)
	end

	-- render gear
	function self:render()
	 for _, part in ipairs(self.limbs) do
		 part:render()
	 end
	end

	
	
	return o

end