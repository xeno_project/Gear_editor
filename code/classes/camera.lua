-- *************************************
-- *                                   *
-- *              Camera               *
-- *                             Class *
-- *************************************
cameraClass = {};function cameraClass:new(o)
	
    -- Constructor
	local function constructor(o)
	
		o = o or {}
		
		-- Base properties
		
		-- Nested properties  
		o.position = {
			x=0,
			y=25,
			z=-135
		}
		o.angle = {
			x=0,
			y=90,
			z=0,
			pitch=0
		}
		
		-- Set metatable
		setmetatable(o, self)    
		self.__index = self

		
		return self

	end;self = constructor(o)

	-- *************************************
	-- *              Methods              *
	-- *************************************	
	-- Initialize camera
	function self:init()

		-- Set camera field of view
		self.lib.camera.fov = math.rad(10)

		-- Update view matrix with current setup
		self.lib.camera.updateViewMatrix()

		-- Update projection matrix with current setup
		self.lib.camera.updateProjectionMatrix()

	end

	-- Update camera
	function self:update()

		-- Update camera look in direction 
		self.lib.camera.lookInDirection(
			self.position.x, 
			self.position.z, 
			self.position.y, 
			math.rad(self.angle.y), 
			math.rad(self.angle.pitch)
		)

	end
	
	return o

end