-- *************************************
-- *                                   *
-- *              GearPart             *
-- *                             Class *
-- *************************************
gearPartClass = {};function gearPartClass:new(o)
	
    -- Constructor
  local function constructor(o)

    o = o or {}

    -- Base properties
    o.name = ""
    o.scale = {}
    o.children = {}
    
    -- Nested properties  
    o.model = {}

    o.baseRotation = {
      x = math.rad(90),
      y = 0,
      z = 0 
    }

    o.rotation = {
      x = math.rad(90),
      y = 0,
      z = 0 
    }

    o.basePosition = {
      x = 0,
      y = 0,
      z = 0
    }

    o.position = {
      x=0,
      y=0,
      z=0
    }
    
    -- Set metatable
    setmetatable(o, self)    
    self.__index = self

    
    return self

  end;self = constructor(o)

  -- *************************************
  -- *              Methods              *
  -- *************************************	
  -- Initialize Part 
  function self:init(name, objectName, textureName, position, scale)
    self.scale = scale
    -- initial part position
    self.position.x = position[1] * scale[1]
    self.position.y = position[2] * scale[2]
    self.position.z = position[3] * scale[3]
    self.basePosition.x = self.position.x
    self.basePosition.y = self.position.y
    self.basePosition.z = self.position.z

    self.name = name

    -- Load part from file
    self:load(objectName, textureName)

  end

  --  
  function self:load(objectName, texture)

    
    -- Create 3D model
    local partPosition = {self.position.x, self.position.y, self.position.z}
    local partRotation = {self.rotation.x, self.rotation.y, self.rotation.z}
    self.model = self.lib.newModel("assets/models/gears/"..objectName, texture, 
      partPosition, 
      partRotation, 
      self.scale
    )
      
  end

  -- Render part
  function self:render()

    -- Set part culling
    love.graphics.setMeshCullMode("none")

    -- Drawpart 
    self.model:draw()

  end

  -- update part position
  function self:updatePosition(x, y, z)
    self.position.x =  x
    self.position.y =  y 
    self.position.z =  z
    self.model:setTranslation(self.position.x, self.position.y, self.position.z)
  end

  -- update part rotation
  function self:updateRotation(x, y, z)
    self.rotation.x = self.rotation.x + x
    self.rotation.y = self.rotation.y + y
    self.rotation.z = self.rotation.z + z
    self.model:setRotation(self.rotation.x, self.rotation.y, self.rotation.z)
  end

  -- forward kinematic 
  function self:transform(x, y, z, rx, ry, rz)
    -- traslation
    x = x or self.position.x
    y = y or self.position.y
    z = z or self.position.z
    self:updatePosition(x, y, z)

    -- rotation
    self:updateRotation(rx, ry, rz)

    -- update children position and rotation
    for _, child in ipairs(self.children) do

      -- delta x, y, z
      local gx = (child.basePosition.x) - self.basePosition.x
      local gy = (child.basePosition.y) - self.basePosition.y
      local gz = (child.basePosition.z) - self.basePosition.z

      -- child rotation 
      local ca, cb, cc = math.cos(self.rotation.z - self.baseRotation.z), math.cos(self.rotation.y - self.baseRotation.y), math.cos(self.rotation.x - self.baseRotation.x)
      local sa, sb, sc = math.sin(self.rotation.z - self.baseRotation.z), math.sin(self.rotation.y - self.baseRotation.y), math.sin(self.rotation.x - self.baseRotation.x)
      local newX = ca * cb * gx + (ca * sb * sc - sa * cc) * gy + (ca * sb * cc + sa * sc) * gz  + self.position.x
      local newY = sa * cb * gx + (sa * sb * sc + ca * cc) * gy + (sa * sb * cc - ca * sc) * gz  + self.position.y
      local newZ = -sb * gx + cb * sc * gy + cb * cc * gz + self.position.z

      -- update child
      child:transform(newX, newY, newZ, rx , ry , rz , false)
    end
  end

  -- add a new child
  function self:addChild(child)
    table.insert(self.children, child)
  end


  return o

end
