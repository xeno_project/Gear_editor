local gear = {
  name = "El Crescens",
  texture = "ElCrescens_texture.png",
  scale = {0.005, 0.005, 0.005},
  limbs = {}
}

gear.limbs[1] = {
  id = 1,
  name = "hip",
  objectName = "ElCrescens_Hip.obj",
  position = {x = 0, y = 0, z = 4255.189},
  parent = nil,
  culling = "none",
}

gear.limbs[2] = {
  id = 2,
  name = "chest",
  objectName = "ElCrescens_Chest.obj",
  position = {x = 0, y = 0, z = 4599.718},
  parent = 1,
  culling = "none",
}

gear.limbs[3] = {
  id = 3,
  name = "left thigh",
  objectName = "ElCrescens_L_Thigh.obj",
  position = {x = 415.303, y = -107.082, z = 4002.757},
  parent = 1,
  culling = "none",
}

gear.limbs[4] = {
  id = 4,
  name = "right thigh",
  objectName = "ElCrescens_R_Thigh.obj",
  position = {x = -415.303, y = -107.082, z = 4002.757},
  parent = 1,
  culling = "none",
}

gear.limbs[5] = {
  id = 5,
  name = "left shin",
  objectName = "ElCrescens_L_Shin.obj",
  position = {x = 319.057, y = -107.082, z = 2705.804},
  parent = 3,
  culling = "none",
}

gear.limbs[6] = {
  id = 6,
  name = "right shin",
  objectName = "ElCrescens_R_Shin.obj",
  position = {x = -319.057, y = -107.082, z = 2705.804},
  parent = 4,
  culling = "none",
}

gear.limbs[7] = {
  id = 7,
  name = "right leg",
  objectName = "ElCrescens_R_Leg.obj",
  position = {x = -319.0570, y = -107.082, z = 2705.804},
  parent = 6,
  culling = "none",
}

gear.limbs[8] = {
  id = 8,
  name = "left leg",
  objectName = "ElCrescens_L_Leg.obj",
  position = {x = 319.0570, y = -107.082, z = 2705.804},
  parent = 5,
  culling = "none",
}

gear.limbs[9] = {
  id = 9,
  name = "right foot",
  objectName = "ElCrescens_R_Foot.obj",
  position = {x = -265.044, y = -107.082, z = 706.703},
  parent = 7,
  culling = "none",
}

gear.limbs[10] = {
  id = 10,
  name = "left foot",
  objectName = "ElCrescens_L_foot.obj",
  position = {x = 265.044, y = -107.082, z = 706.703},
  parent = 8,
  culling = "none",
}

gear.limbs[11] = {
  id = 11,
  name = "head",
  objectName = "ElCrescens_Head.obj",
  position = {x = 0, y = -62.92, z = 5827.696},
  parent = 2,
  culling = "none",
}

return gear