-- *************************************
-- *                                   *
-- *            Gear editor            *
-- *                                   *
-- *************************************

-- Controls
love.keyboard.setKeyRepeat(true)

-- Mouse
love.mouse.setVisible(false)

-- Filters
love.graphics.setDefaultFilter("nearest", "nearest", 1)
love.graphics.setLineStyle("rough")

-- Fonts
font = love.graphics.newImageFont("assets/img/misc/font.png",
" abcdefghijklmnopqrstuvwxyz" ..
"ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..
"123456789.,!?-+/():;%&`'*#=[]\"")	
font:setLineHeight(0.88)

font2 = love.graphics.newImageFont("assets/img/misc/font2.png",
" abcdefghijklmnopqrstuvwxyz" ..
"ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..
"123456789.,!?-+/():;%&`'*#=[]\"")	
    
-- Set font
love.graphics.setFont(font)
    
-- Avoid blur/smoothing
font:setFilter( "nearest", "nearest" )
font2:setFilter( "nearest", "nearest" )