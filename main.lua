-- *************************************
-- *                                   *
-- *            Gear editor            *
-- *                                   *
-- *************************************
local g3d = require "code/libraries/g3d"

-- Includes 
require "code/init"
require "code/classes/debug"
require "code/classes/camera"
require "code/classes/gear"

-- *************************************
-- *                                   *
-- *           Load callback           *
-- *                                   *
-- *************************************
function love.load()

	-- Instanciate debug class
	debug = debugClass:new{}	 

	-- Instanciate camera class
	camera = cameraClass:new{lib = g3d}	 
	camera:init()

	-- Instanciate gear class
	gear = gearClass:new{lib = g3d}	 
	gear:init("code/data/gears/ElCrescens") 
	debug:setName(gear.name)
	
end


-- *************************************
-- *                                   *
-- *          Update callback          *
-- *                                   *
-- *************************************
function love.update(dt)
	
	-- *************************************
	-- *           Update camera           *
	-- *************************************
	camera:update()
	
end


-- *************************************
-- *                                   *
-- *           Draw callback           *
-- *                                   *
-- *************************************
function love.draw()
    
	-- *************************************
	-- *           Render world            *
	-- *************************************
	-- Render models
	gear:render() -- Must be refactored

	-- *************************************
	-- *            Render UI              *
	-- *************************************
	-- Here goes ui 

	-- *************************************
	-- *         Render debug data         *
	-- *************************************
	-- Display camera data
	debug:displayCameraData(camera, 10, 10)
	-- Display gear name
	debug:displayGearName(280, 10)
	-- Display gear data
	-- debug:displayGearData()

end


-- *************************************
-- *                                   *
-- *         Control callbacks         *
-- *                                   *
-- *************************************
-- Keypress function
function love.keypressed(key,scancode,isrepeat)
	
	if key == "right" then
		camera.position.x = camera.position.x + 0.5
	elseif key == "left" then
		camera.position.x = camera.position.x - 0.5
	elseif key == "up" then
		camera.position.z = camera.position.z + 1 
	elseif key == "down" then
		camera.position.z = camera.position.z - 1

	-- limbs movements test	
	elseif key == "w" then
		gear:rotateLimb(4, math.rad(10), 0 ,0)
	elseif key == "a" then
		gear:rotateLimb(4, 0, math.rad(10),0)
	elseif key == "s" then
		gear:rotateLimb(4, -math.rad(10), 0 ,0)
	elseif key == "d" then
		gear:rotateLimb(4, 0, -math.rad(10),0)
	elseif key == "space" then
		gear:moveGear(0, 0, 0.5)
	elseif key == "lshift" then
		gear:moveGear(0, 0, -0.5)
	end
	
end

-- Keyreleased function
function love.keyreleased(key,scancode)
end